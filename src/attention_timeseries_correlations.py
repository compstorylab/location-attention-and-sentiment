import plotly.express as px
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import timedelta
from datetime import datetime
from scipy import stats
us_state_to_abbrev = {
    "Alabama": "AL",
    "Alaska": "AK",
    "Arizona": "AZ",
    "Arkansas": "AR",
    "California": "CA",
    "Colorado": "CO",
    "Connecticut": "CT",
    "Delaware": "DE",
    "Florida": "FL",
    "Georgia": "GA",
    "Hawaii": "HI",
    "Idaho": "ID",
    "Illinois": "IL",
    "Indiana": "IN",
    "Iowa": "IA",
    "Kansas": "KS",
    "Kentucky": "KY",
    "Louisiana": "LA",
    "Maine": "ME",
    "Maryland": "MD",
    "Massachusetts": "MA",
    "Michigan": "MI",
    "Minnesota": "MN",
    "Mississippi": "MS",
    "Missouri": "MO",
    "Montana": "MT",
    "Nebraska": "NE",
    "Nevada": "NV",
    "New Hampshire": "NH",
    "New Jersey": "NJ",
    "New Mexico": "NM",
    "New York": "NY",
    "North Carolina": "NC",
    "North Dakota": "ND",
    "Ohio": "OH",
    "Oklahoma": "OK",
    "Oregon": "OR",
    "Pennsylvania": "PA",
    "Rhode Island": "RI",
    "South Carolina": "SC",
    "South Dakota": "SD",
    "Tennessee": "TN",
    "Texas": "TX",
    "Utah": "UT",
    "Vermont": "VT",
    "Virginia": "VA",
    "Washington": "WA",
    "West Virginia": "WV",
    "Wisconsin": "WI",
    "Wyoming": "WY",
    "District of Columbia": "DC",
    "American Samoa": "AS",
    "Guam": "GU",
    "Northern Mariana Islands": "MP",
    "Puerto Rico": "PR",
    "United States Minor Outlying Islands": "UM",
    "Virgin Islands": "VI",
}

def plot_map(fname, title, freq='2D'):
    df = pd.read_csv('../data/location_attention/'+fname, index_col = [0,1], sep='\t', parse_dates=[0])

    print(df)
    x = df.groupby(level='_id', group_keys=True)['freq'].rolling(window=7, center=True).mean()
    x.index = x.index.droplevel(level=0)
    x = x.rename("freq_smoothed")

    df = df.merge(x, left_index=True, right_index=True)

    df['log_freq'] = np.log10(df['freq']+0.0000001)
    df['log_freq_smoothed'] = np.log10(df['freq_smoothed']+0.0000001)
    df['cum_freq_smoothed'] = df.groupby(level=-1)['freq_smoothed'].cumsum()
    level_values = df.index.get_level_values
    df1 = df.groupby([level_values(1)]+[pd.Grouper(freq=freq, level=0)]).mean()
    #df['cum_freq_smoothed'] = df.groupby([level_values(1)]+
    #                                     [pd.Grouper(freq=freq, level=0)])['freq_smoothed'].cumsum()
    df['cum_freq_smoothed'] = df.groupby(level_values(1)
                                         )['freq_smoothed'].cumsum()

    df2 = pd.DataFrame(df1.to_records())
    df2.fillna({'freq':0, 'freq_smoothed':0, 'log_freq':-6, 'log_freq_smoothed':-6}, inplace=True)
    df2['date'] = [i.strftime('%Y-%m-%d') for i in pd.to_datetime(df2['level_0'])]
    #df2.groupby('_id').sum()

    fig = px.choropleth(df2,
              locations = '_id',
              color="freq_smoothed",
              color_continuous_scale="Magma",
              locationmode='USA-states',
              scope="usa",
              range_color=(-4, -1),
              title=title,
              height=600
             )


    fig.write_html(f'../figures/{title}.html')

    fig.show()

    return df

